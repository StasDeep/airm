#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""Module for logging settings."""

import logging
import sys

import airm._constants as constants


def set_logger():
    """Set up logger with console and file handlers."""
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    root.addHandler(get_console_handler())
    root.addHandler(get_file_handler())


def set_silent():
    """Remove console handler.
    This function is used to turn console output off,
    which means program is running silent.

    """
    root = logging.getLogger()
    root.handlers = []
    root.addHandler(get_file_handler())


def set_dry():
    """Remove file handler.
    This function is used to turn file output off.
    It is used in dry run.

    """
    root = logging.getLogger()
    root.handlers = []
    root.addHandler(get_console_handler())


def get_console_handler():
    """Return console logging handler."""
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    console_handler.setFormatter(formatter)
    return console_handler

def get_file_handler():
    """Return file logging handler."""
    file_handler = logging.FileHandler(constants.LOG_PATH)
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(asctime)s] - %(levelname)s: %(message)s')
    file_handler.setFormatter(formatter)
    return file_handler
