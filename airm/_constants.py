#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""Constant values for airm program. Most of constants are paths."""

import os

PROGRAM_NAME = 'airm'

CONFIG_FILE_NAME = '.{}_config.json'.format(PROGRAM_NAME)
CONFIG_DIR = os.path.expanduser('~')
CONFIG_PATH = os.path.join(CONFIG_DIR, CONFIG_FILE_NAME)

LOG_FILE_NAME = '.{}.log'.format(PROGRAM_NAME)
LOG_DIR = os.path.expanduser('~')
LOG_PATH = os.path.join(LOG_DIR, LOG_FILE_NAME)

TRASH_DIR_NAME = '.{}_trash'.format(PROGRAM_NAME)
DEFAULT_TRASH_DIR = os.path.expanduser('~')
DEFAULT_TRASH_PATH = os.path.join(DEFAULT_TRASH_DIR, TRASH_DIR_NAME)

TRASH_BASE_NAME = '.{}_files.json'.format(PROGRAM_NAME)
TRASH_BASE_DIR = DEFAULT_TRASH_PATH
DEFAULT_TRASH_BASE_PATH = os.path.join(TRASH_BASE_DIR, TRASH_BASE_NAME)

DEFAULT_CONFIG = dict(
    trash_path=DEFAULT_TRASH_PATH,
    time_policy=30,     # Days
    size_policy=1024,   # Megabytes
    count_policy=None
)

MAX_LIST_HEIGHT = 40

POLICIES = [attr + '_policy' for attr in ['time', 'size', 'count']]

SUCCESS_CODE = 0
INIT_FAIL_CODE = 1
CLEAN_FAIL_CODE = 2
RECOVER_FAIL_CODE = 3
CONFIG_FAIL_CODE = 4
RM_FAIL_CODE = 5
