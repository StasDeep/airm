#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""AIRM trash.
Trash module is a module for manipulating trashbin.

"""

import logging
import os

import airm.util as util
import airm.config as config
import airm._constants as constants
import airm._logger as logger
import airm._trash_base as trash_base


def get_trash_files(
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_path=constants.CONFIG_PATH):
    """Return list of filedicts in trashbin.
    Reads config and gets trashbin path, where it reads filebase from.

    Args:
        trash_dir (str): path of trashbin.
        time_policy (int): number of days after that files are removed.
        size_policy (int): maximum size of trashbin in megabytes.
        count_policy (int): maximum amount of files in trashbin.
        config_path (str): path of config file.

    Returns:
        list: files as dicts.

    Raises:
        OSError: if custom config does not exist.
        ValueError: if config is invalid.

    """
    config_dict = config.load_config(config_path)

    util.extend_dict(config_dict,
                     trash_path=trash_dir,
                     time_policy=time_policy,
                     size_policy=size_policy,
                     count_policy=count_policy)

    trash_path = config_dict['trash_path']

    return trash_base.load_trash_base(trash_path)


def show(
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_path=constants.CONFIG_PATH):
    """Show files in trashbin.

    Args:
        trash_dir (str): path of trashbin.
        time_policy (int): number of days after that files are removed.
        size_policy (int): maximum size of trashbin in megabytes.
        count_policy (int): maximum amount of files in trashbin.
        config_path (str): path of config file.

    Raises:
        OSError: if custom config does not exist.
        ValueError: if config is invalid.

    """
    config_dict = config.load_config(config_path)

    util.extend_dict(config_dict,
                     trash_path=trash_dir,
                     time_policy=time_policy,
                     size_policy=size_policy,
                     count_policy=count_policy)

    trash_path = config_dict['trash_path']

    try:
        filedicts = trash_base.load_trash_base(trash_path)
    except OSError, IOError:
        return

    trash_base.apply_policies(config_dict, filedicts)
    trash_base.write_trash_base(trash_path, filedicts)

    if not filedicts:
        print 'Trashbin is empty'
        return
    _print_files(filedicts)


def clean(
        filenames,
        take_all=True,
        interactive_run=False,
        dry_run=False,
        silent_run=True,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_path=constants.CONFIG_PATH):
    """Permanently remove files from trashbin.

    Args:
        filenames (list): list with names of trashbin files to be cleaned.
        take_all (bool): if True, solve name conflict, grabbing all files.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        silent_run (bool): if True, run without output.
        trash_dir (str): path of trashbin.
        time_policy (int): number of days after that files are removed.
        size_policy (int): maximum size of trashbin in megabytes.
        count_policy (int): maximum amount of files in trashbin.
        config_path (str): path of config file.

    Raises:
        TypeError: if filenames are not in list or tuple.
        ValueError: if config is invalid.
        OSError: if custom config does not exist.

    Returns:
        dict: list of dicts with filename and error message.

    """
    if isinstance(filenames, str):
        raise TypeError('filenames must be list or tuple')

    if silent_run:
        logger.set_silent()

    if dry_run:
        logger.set_dry()
        logging.warning('running in dry mode, nothing will be changed')

    config_dict = config.load_config(config_path)

    util.extend_dict(config_dict,
                     trash_path=trash_dir,
                     time_policy=time_policy,
                     size_policy=size_policy,
                     count_policy=count_policy)

    trash_path = config_dict['trash_path']

    try:
        all_filedicts = trash_base.load_trash_base(trash_path)
    except OSError, IOError:
        return util.resulting_dict('trash_path is invalid')

    filedicts, result = _get_filedicts(
        filenames, take_all, all_filedicts)

    for filedict in filedicts[:]:
        _clean_file(
            filedict=filedict,
            interactive_run=interactive_run,
            dry_run=dry_run,
            trash_path=trash_path,
            filedicts=all_filedicts)
        result.append(dict(name=filedict['name'],
                           msg='',
                           time=filedict['time']))

    policy_files = trash_base.apply_policies(config_dict, all_filedicts)
    trash_base.write_trash_base(trash_path, all_filedicts)

    return util.resulting_dict(result, policy_files)


def recover(
        filenames,
        take_all=True,
        interactive_run=False,
        dry_run=False,
        silent_run=True,
        create_dir=True,
        suffix=True,
        replace=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_path=constants.CONFIG_PATH):
    """Recover files from trashbin to their original locations.

    Args:
        filenames (list): list with names of trashbin files to be cleaned.
        take_all (bool): if True, solve name conflict, grabbing all files.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        silent_run (bool): if True, run without output.
        create_dir (bool): if True, always create original directory
            if it no longer exists.
        suffix (bool): if True, always add suffix to the end of name,
            if other file with this name already exists.
        replace (bool): if True, always replace file,
            if other file with this name already exists.
        trash_dir (str): path of trashbin.
        time_policy (int): number of days after that files are removed.
        size_policy (int): maximum size of trashbin in megabytes.
        count_policy (int): maximum amount of files in trashbin.
        config_path (str): path of config file.

    Raises:
        TypeError: if filenames are not in list or tuple.
        ValueError:
            - if config is invalid.
            - if suffix and replace are both True.
        OSError: if custom config does not exist.

    Returns:
        dict: list of dicts with filename and error message.

    """
    if isinstance(filenames, str):
        raise TypeError('filenames must be list or tuple')

    if suffix and replace:
        raise ValueError('suffix and replace cannot both be True')

    if silent_run:
        logger.set_silent()

    if dry_run:
        logger.set_dry()
        logging.warning('running in dry mode, nothing will be changed')


    config_dict = config.load_config(config_path)

    util.extend_dict(config_dict,
                     trash_path=trash_dir,
                     time_policy=time_policy,
                     size_policy=size_policy,
                     count_policy=count_policy)

    trash_path = config_dict['trash_path']

    try:
        all_filedicts = trash_base.load_trash_base(trash_path)
    except OSError, IOError:
        return util.resulting_dict('trash_path is invalid')

    filedicts, result = _get_filedicts(
        filenames, take_all, all_filedicts)

    for filedict in filedicts[:]:
        _recover_file(
            filedict=filedict,
            create_dir=create_dir,
            suffix=suffix,
            replace=replace,
            interactive_run=interactive_run,
            dry_run=dry_run,
            trash_path=trash_path,
            filedicts=all_filedicts)
        result.append(dict(name=filedict['name'],
                           msg='',
                           time=filedict['time']))

    policy_files = trash_base.apply_policies(config_dict, all_filedicts)
    trash_base.write_trash_base(trash_path, all_filedicts)

    return util.resulting_dict(result, policy_files)


def _get_filedicts(filenames, take_all, all_filedicts):
    """Return filedicts according according to names.
    In other words, return trashbin files filtered by name.

    Args:
        filenames (list): list with names of trashbin files to be taken.
        take_all (bool): if True, solve name conflict, grabbing all files.
        all_filedicts (list): list of all files in trashbin as dicts.

    Returns:
        tuple: (filtered list of filedicts, errors).

    """
    # If the list is empty, user wants to take all files and influence them.
    if not filenames:
        return (all_filedicts, [])

    filedicts = []
    errors = []

    for filename in filenames:
        if isinstance(filename, basestring):
            files_with_current_name = [filedict for filedict in all_filedicts
                                       if filedict['name'] == filename]
        else:
            files_with_current_name = [filedict for filedict in all_filedicts
                                       if filedict['name'] == filename[0]
                                       and filedict['time'] == filename[1]]

        if len(files_with_current_name) == 0:
            if isinstance(filename, basestring):
                name = filename
            else:
                name = filename[0]

            errors.append(dict(
                name=name,
                msg='there is no such file/directory in trashbin',
                time=''
            ))

        if len(files_with_current_name) > 1 and not take_all:
            files_with_current_name = _prompt_file_choice(files_with_current_name)
        filedicts.extend(files_with_current_name)

    return (filedicts, errors)


def _clean_file(
        filedict,
        interactive_run,
        dry_run,
        trash_path,
        filedicts):
    """Permanently remove file from trash.

    Args:
        filedict (dict): file as dict with neccessary attributes.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        trash_path (str): path of trashbin.
        filedicts (list): all trashbin files.

    """
    prompt = 'Permanently remove \'{}\'?'.format(filedict['name'])

    if not interactive_run or util.yes_no_prompt(prompt):
        if not dry_run:
            trash_base.remove_file_from_trash(filedicts,
                                              filedict,
                                              trash_path)

        logging.info('permanently removing \'{}\' from trashbin'\
                     .format(filedict['name']))


def _recover_file(
        filedict,
        create_dir,
        suffix,
        replace,
        interactive_run,
        dry_run,
        trash_path,
        filedicts):
    """Recover file to its original path.

    Args:
        filedict (dict): file as dict with neccessary attributes.
        create_dir (bool): if True, always create original directory
            if it no longer exists.
        suffix (bool): if True, always add suffix to the end of name,
            if other file with this name already exists.
        replace (bool): if True, always replace file,
            if other file with this name already exists.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        silent_run (bool): if True, run without output.
        trash_path (str): path of trashbin.
        filedicts (list): all trashbin files.

    """
    orig_dir = filedict['dir']
    orig_path = os.path.join(orig_dir, filedict['name'])

    # If original location no longer exists, prompt to create it anew.
    if not os.path.exists(orig_dir):
        # If --create-dir flag is passed, don't prompt.
        if create_dir or _prompt_dir_creation(orig_dir):
            os.makedirs(orig_dir)
        else:
            logging.warning('not recovering \'{}\''.format(filedict['name']))
            return

    # If file with original name already exists, prompt to replace it.
    if os.path.exists(orig_path):
        # If neither --replace nor --suffix is passed, prompt.
        if not replace and not suffix:
            replace_file = _prompt_file_replace(orig_path)
        elif replace:
            replace_file = True
        elif suffix:
            replace_file = False

        # If not replacing file, add suffix.
        if not replace_file:
            new_path = orig_path
            suffix = 1
            while os.path.exists(new_path):
                new_path = '{} ({})'.format(orig_path, suffix)
                suffix += 1
            orig_path = new_path

    prompt = 'Recover \'{}\' to \'{}\'?'\
             .format(filedict['name'], filedict['dir'])

    if not interactive_run or util.yes_no_prompt(prompt):
        if not dry_run:
            trash_base.move_file_from_trash(filedicts,
                                            filedict,
                                            orig_path,
                                            trash_path)

        logging.info('recovering \'{}\' from trashbin to \'{}\''\
                     .format(filedict['name'], orig_dir))


def _prompt_dir_creation(path):
    """Decide whether to create directory.
    Ask user if it's necessary to create original location
    if it no longer exists.

    Args:
        path (str): path to original location.

    Returns:
        bool: True, if user answers positively.

    """
    prompt = 'Directory \'{}\' where from file was removed no longer exists. '\
             'Do you want to create it?'.format(path)
    return util.yes_no_prompt(prompt)


def _prompt_file_replace(path):
    """Decide whether to replace file if one with such filename exists..

    Args:
        path (str): path to file which already exists and can be replaced.

    Returns:
        bool: True, if user answers positively.

    """
    prompt = 'File/directory \'{}\' already exists. '\
             'You can either replace it or add suffix to name. '\
             'Do you want to replace it?'.format(path)
    return util.yes_no_prompt(prompt)


def _prompt_file_choice(filedicts):
    """Return files to be taken.
    Decide which version(s) of file are to be cleaned/recovered,
    if there are several ones with the same name.

    If 0 is entered, nothing is taken.
    If nothing is entered, all files with the name are taken.

    Args:
        filedicts (list): files to choose from.

    Returns:
        list: files which were taken.

    """
    index = None
    print 'Name conflict occured.'
    _print_files(filedicts)
    print 'Please, choose file version (enter number) or '\
          'leave it empty to take all or enter 0 to take nothing:'

    while index is None:
        index = raw_input('> ')
        index = _validated_index(index, len(filedicts))
        if index == '':
            return filedicts
        elif index == 0:
            return []
        else:
            return [filedicts[index-1]]


def _validated_index(index, length):
    """Validate index in file version choice.

    Returns:
        int: index of a files to be taken.
        None: if index is invalid.
        '': if user entered nothing
        .
    """
    if index.isdigit():
        index = int(index)

    if range(0, length+1).count(index) or index == '':
        return index
    else:
        print '{} version not found. Choose again:'.format(repr(index))
        return None


def _print_files(filedicts):
    """Print files.
    If there are more than MAX_LIST_HEIGHT files,
    show it with pydoc.pager as in less command.

    Args:
        filedicts (list): list with files as dicts.

    """
    max_name_width = max(len(x['name']) for x in filedicts)
    max_size_width = max(len(str(x['size'])) for x in filedicts)
    max_index_width = len(str(len(filedicts)))
    trash_contents = ['Total: {}'.format(len(filedicts))]

    pattern = ('{index:{i_width}}. {name:{n_width}}  '
               '{type:5} {size:{s_width}}  {time}')

    for i, filedict in enumerate(filedicts):
        line = (pattern).format(index=i+1,
                                name=filedict['name'],
                                type=filedict['type'],
                                size=filedict['size'],
                                time=filedict['time'].split('.')[0],
                                s_width=max_size_width,
                                i_width=max_index_width,
                                n_width=max_name_width)

        trash_contents.append(line)

    files_list = '\n'.join(trash_contents)

    if len(filedicts) > constants.MAX_LIST_HEIGHT:
        from pydoc import pager
        pager(files_list)
    else:
        print files_list
