#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""Trash Base module is made for manipulating trashbin."""

import datetime
import json
import os

import airm.util as util
import airm.config as config
import airm._constants as constants


# ===== Base Read & Write =====

def load_trash_base(trash_path):
    """Load files and return them as list.

    Args:
        trash_path (str): path, where trashbin is located.

    Returns:
        list: files, loaded from trash_base, as dicts.

    Raises:
        OSError: if trash_path is not a dir.

    """
    if os.path.exists(trash_path) and not os.path.isdir(trash_path):
        raise OSError('trashbin is not a directory')

    trash_base_path = os.path.join(trash_path, constants.TRASH_BASE_NAME)

    if not os.path.exists(trash_base_path):
        trash_base_dir = os.path.dirname(trash_base_path)
        if not os.path.exists(trash_base_dir):
            os.makedirs(trash_base_dir)

        write_trash_base(trash_path)

    with open(trash_base_path) as infile:
        trash_base = json.load(infile)['files']

    return trash_base


def write_trash_base(trash_path, trash_base=None):
    """Write files to JSON.

    Args:
        trash_path (str): path, where trashbin is located.
        trash_base (list): list of files as dicts.

    """
    trash_base_path = os.path.join(trash_path, constants.TRASH_BASE_NAME)

    if trash_base is None:
        trash_base = []

    data = dict(files=trash_base)
    with open(trash_base_path, 'w') as outfile:
        json.dump(data, outfile, indent=4)


# ===== Getters =====

def get_path_in_trash(trash_path, filedict):
    """Return path of the file in trashbin."""
    filename_in_trash = filedict['name'] + filedict['time']
    path_in_trash = os.path.join(trash_path, filename_in_trash)
    return path_in_trash


# ===== Base Manipulations =====

def move_file_to_trash(path, trash_path):
    """Move file to trashbin.

    Args:
        path (str): path of a file to be moved.
        trash_path (str): path of a trashbin.

    """
    filedict = util.get_file_info(path)
    new_path = get_path_in_trash(trash_path, filedict)

    try:
        util.move(path, new_path)
    except OSError:
        raise OSError('permission denied')

    return filedict['time']


def extend_trash(trash_base, filedicts):
    """Append non-erroneous files to list of all files in trashbin.

    Args:
        trash_base (list): list of files as dicts that are inside trashbin.
        filedicts (list): list of files as dicts to be put into trashbin.

    """
    for filedict in filedicts:
        if filedict['msg'] == '':
            trash_base.append(dict((key, filedict[key])
                              for key in filedict if key != 'msg'))


def remove_file_from_trash(trash_base, filedict, trash_path):
    """Remove file from trashbin.

    Args:
        trash_base (list): list of files as dicts.
        filedict (dict): dict, containing info about file, which is
            to be removed from trash.
        trash_path (str): path of a trashbin.

    """
    file_path = get_path_in_trash(trash_path, filedict)
    try:
        util.remove(file_path)
    except OSError:
        raise OSError('permission denied')
    trash_base.remove(filedict)


def move_file_from_trash(trash_base, filedict, orig_path, trash_path):
    """Move file from trashbin to its original location.

    Args:
        trash_base (list): list of files as dicts.
        filedict (dict): dict, containing info about file, which is
            to be moved from trash.
        orig_path(str): path where file will be moved to.
        trash_path (str): path of a trashbin.

    """
    path_in_trash = get_path_in_trash(trash_path, filedict)

    try:
        util.move(path_in_trash, orig_path)
    except OSError:
        raise OSError('permission denied')
    trash_base.remove(filedict)


# ===== Policies =====

def apply_policies(config_dict, trash_base):
    """Remove files from trashbin by different policies: time, size, count.

    Args:
        config_path (str): path of a config file to load policies.
        trash_base (list): list of files as dicts.

    Returns:
        list: list of removed files.

    """
    trash_path = config_dict['trash_path']
    time_policy = config_dict['time_policy']
    size_policy = config_dict['size_policy']
    count_policy = config_dict['count_policy']
    result = []

    if time_policy:
        result += apply_time_policy(time_policy, trash_base, trash_path)
    if size_policy:
        result += apply_size_policy(size_policy, trash_base, trash_path)
    if count_policy:
        result += apply_count_policy(count_policy, trash_base, trash_path)

    return result


def apply_time_policy(time, trash_base, trash_path):
    """Remove old files.

    Args:
        time (int): number of days after that files are removed.
        trash_base (list): list of files as dicts.

    Returns:
        list: list of removed files.

    """
    result = []
    for filedict in trash_base[:]:
        removed_at = datetime.datetime.strptime(filedict['time'],
                                                '%Y-%m-%d %H:%M:%S.%f')
        current_time = datetime.datetime.now()

        if (current_time - removed_at).days > time:
            trash_base.remove(filedict)
            util.remove(get_path_in_trash(trash_path, filedict))
            result.append(filedict)

    return result


def apply_size_policy(size, trash_base, trash_path):
    """Remove old files until trashbin is light enough.

    Args:
        size (int): maximum size of trashbin in megabytes.
        trash_base (list): list of files as dicts.

    Returns:
        list: list of removed files.

    """
    # Convert from megabytes to bytes.
    size *= 1024**2
    sum_size = sum([filedict['size'] for filedict in trash_base])
    result = []

    while sum_size > size and trash_base:
        sum_size -= trash_base[0]['size']
        filedict = trash_base.pop(0)
        util.remove(get_path_in_trash(trash_path, filedict))
        result.append(filedict)

    return result


def apply_count_policy(count, trash_base, trash_path):
    """Remove old files until trashbin contains accepted amount of files.

    Args:
        count (int): maximum amount of files in trashbin.
        trash_base (list): list of files as dicts.

    Returns:
        list: list of removed files.

    """
    result = []
    diff = len(trash_base) - count
    for _ in range(diff):
        filedict = trash_base.pop(0)
        util.remove(get_path_in_trash(trash_path, filedict))
        result.append(filedict)

    return result
