#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""HRF stands for Human Readable Format.
This module gives main functions to work with it.

HRF supports three types: int, str and NoneType.

Examples:
    >>> print dumps(dict(foo=1, bar='word', baz=None))
    Foo: 1
    Bar: "word"
    Baz: -

    >> print dumps(dict(foo_bar_baz=0))
    Foo Bar Baz: 0

"""

def load(infile):
    """Deserialize HRF file to dict.

    Args:
        infile (file): file with HRF key-value pairs.

    Returns:
        dict: dictionary with corresponding key-value pairs.

    Raises:
        ValueError:
            - pair does not exist (only key given).
            - literals are invalid
            - keys are not unique

    """
    return _load_from_iterable(infile)


def loads(text):
    """Deserialize HRF string to dict.

    Args:
        text (str): string with key-value pairs divided with \n symbol.

    Returns:
        dict: dictionary with corresponding key-value pairs.

    Raises:
        ValueError:
            - pair does not exist (only key given).
            - literals are invalid
            - keys are not unique

    """
    return _load_from_iterable(text.split('\n'))


def _load_from_iterable(iterable):
    """Deserialize HRF iterable (list of strings or file) to dict."""
    data = dict()
    for line in iterable:
        if line.strip() == '':
            continue

        # This statement raises ValueError if there is no ':' char.
        key, value = [part.strip() for part in line.split(':', 1)]

        key = '_'.join(key.split()).lower()
        if value == '-':
            value = None
        elif value.startswith('"') and value.endswith('"'):
            value = value[1:-1]
        elif value.isdigit():
            value = int(value)
        else:
            raise ValueError('invalid HRF literals')

        if key in data:
            raise ValueError('HRF keys must be unique')

        data[key] = value

    return data


def dump(data, outfile):
    """Serialize dict to HRF file.

    Args:
        data (dict): dict to be written into file.
        outfile (file): file where data is written to.

    Raises:
        ValueError: if data contains unsupported type.

    """
    outfile.write(dumps(data))


def dumps(data):
    """Serialize dict to HRF string.

    Args:
        data (dict): dict to be converted to string.

    Returns:
        str: serialized dictionary in HRF.

    Raises:
        ValueError: if data contains unsupported type.

    """
    serialized = []
    for key in data:
        key_str = ' '.join(word.capitalize() for word in key.split('_'))
        value = data[key]

        if isinstance(value, int):
            pass
        elif isinstance(value, basestring):
            value = '"{}"'.format(value)
        elif value is None:
            value = '-'
        else:
            raise ValueError('dictionary contains unsupported type')

        serialized.append('{}: {}'.format(key_str, value))
    return '\n'.join(sorted(serialized))
