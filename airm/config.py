#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""AIRM config.
Config module is made for manipulating config and for storing parameters.

"""

import logging
import json
import os

import airm.util as util
import airm._constants as constants
import airm.hrf as hrf
import airm._logger as logger


def show(config_path=constants.CONFIG_PATH):
    """Show config in HRF (human readable format).

    Args:
        config_path (str): path of config file.

    """
    config = load_config(config_path)

    print hrf.dumps(config)


def move_trash(
        new_trash_path,
        config_path=constants.CONFIG_PATH,
        silent_run=True):
    """Move trashbin to new path.

    Args:
        new_trash_path (str): new path of trashbin.
        config_path (str): path of config file.
        silent_run (bool): if True, no output to console.

    Raises:
        ValueError: if custom config is invalid.
        OSError:
            - original trashbin does not exist.
            - trashbin is already in new_trash_path.
            - parent directory of new_trash_path does not exist.
            - new_trash_path is not empty.
            - custom config does not exist.

    """
    if silent_run:
        logger.set_silent()

    config = load_config(config_path)

    old_trash_path = config['trash_path']
    new_trash_path = os.path.abspath(new_trash_path)
    new_trash_dir = os.path.dirname(new_trash_path)

    if not os.path.exists(old_trash_path):
        raise OSError('original trashbin does not exist')

    if old_trash_path == new_trash_path:
        raise OSError('trashbin is already in {}'.format(new_trash_path))

    if not os.path.exists(new_trash_dir):
        message = 'parent directory \'{}\' does not exist'\
                  .format(new_trash_dir)
        raise OSError(message)

    if os.path.exists(new_trash_path) and any(os.listdir(new_trash_path)):
        raise OSError('directory {} is not empty'.format(new_trash_path))

    util.move(old_trash_path, new_trash_path)
    config['trash_path'] = new_trash_path
    _write_config(config_path, config)

    logging.info('moving trashbin to {}'.format(new_trash_path))


def set_time_policy(
        value,
        config_path=constants.CONFIG_PATH,
        silent_run=True):
    """Set time policy.

    Args:
        value (int): number of days after that files are removed.
        config_path (str): path of config file.
        silent_run (bool): if True, no output to console.

    Raises:
        ValueError:
            - if custom config is invalid.
            - if value is invalid.
        OSError: if custom config does not exist.

    """
    _set_policy('time_policy', value, config_path, silent_run)
    logging.info('setting time policy to value {}'.format(value))


def set_size_policy(
        value,
        config_path=constants.CONFIG_PATH,
        silent_run=True):
    """Set size policy.

    Args:
        value (int): maximum size of trashbin in megabytes.
        config_path (str): path of config file.
        silent_run (bool): if True, no output to console.

    Raises:
        ValueError:
            - if custom config is invalid.
            - if value is invalid.
        OSError: if custom config does not exist.

    """
    _set_policy('size_policy', value, config_path, silent_run)
    logging.info('setting size policy to value {}'.format(value))


def set_count_policy(
        value,
        config_path=constants.CONFIG_PATH,
        silent_run=True):
    """Set count policy.

    Args:
        value (int): maximum amount of files in trashbin.
        config_path (str): path of config file.
        silent_run (bool): if True, no output to console.

    Raises:
        ValueError:
            - if custom config is invalid.
            - if value is invalid.
        OSError: if custom config does not exist.

    """
    _set_policy('count_policy', value, config_path, silent_run)
    logging.info('setting count policy to value {}'.format(value))


def _set_policy(
        policy_key,
        policy_value,
        config_path,
        silent_run=True):
    """Set policy value for a specific key.
    If value is 0, sets to None.

    Args:
        policy_key (str): should be one of:
            'time_policy', 'size_policy', 'count_policy'
        policy_value (int): value of the policy (>= 0).
        config_path (str): path of config file.
        silent_run (bool): if True, no output to console.

    """
    if silent_run:
        logger.set_silent()

    config = load_config(config_path)

    _validate_policy(policy_value)

    if policy_value == 0:
        policy_value = None

    config[policy_key] = policy_value

    _write_config(config_path, config)


def load_config(path):
    """Load config from JSON or HRF file and return it.

    Args:
        path (str): path of config file.

    Returns:
        dict: dictionary that contains neccessary config parameters:

    """
    config = dict()

    if os.path.isdir(path):
        raise OSError('config cannot be a directory: \'{}\' is a directory'.format(path))

    if not os.path.exists(path):
        if path != constants.CONFIG_PATH:
            raise OSError('file \'{}\' does not exist'.format(path))

        config_dir = os.path.dirname(path)
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)

        _write_config(path)

    with open(path) as infile:
        try:
            config = json.load(infile)
        except ValueError:
            try:
                # Move to the beginning of file.
                infile.seek(0, 0)
                config = hrf.load(infile)
            except ValueError:
                raise ValueError('invalid config format (only JSON and HRF are supported)')

    _validate_config(config)

    return config


def _write_config(path, config=None):
    """Write config to JSON file.

    Args:
        path (str): path of config file.
        config (dict): dict with neccessary parameters.
            If config is None, default config is written.

    """
    if config is None:
        config = constants.DEFAULT_CONFIG
    with open(path, 'w') as outfile:
        json.dump(config, outfile, indent=4)


def get_trash_path(config_path=constants.CONFIG_PATH):
    """Return full path of trashbin.

    Args:
        config_path (str): path of config file.

    Returns:
        str: absolute path of trashbin.

    Raises:
        ValueError: if custom config is invalid.
        OSError: if custom config does not exist.

    """
    config = load_config(config_path)
    return config['trash_path']


def get_policies(config_path=constants.CONFIG_PATH):
    """Return dict with time, size and count policies.

    Args:
        config_path (str): path of config file.

    Returns:
        dict: dictionary with policies names and values.

    Raises:
        ValueError: if custom config is invalid.
        OSError: if custom config does not exist.

    """
    config = load_config(config_path)
    config.pop('trash_path')
    return config


def _validate_config(config):
    """Validate config for neccessary fields and their values.

    Args:
        config (dict): dictionary with config parameters.

    Raises:
        ValueError:
            - if trash_path is not specified.
            - if policy is not valid.

    """
    if 'trash_path' not in config:
        raise ValueError('trashbin path is not specified in config')

    for policy in constants.POLICIES:
        if policy in config:
            _validate_policy(config[policy])

            # When policy is None, it is skipped
            # whilst zero policy value will lead to full trashbin clean
            # every time something is removed.
            if config[policy] == 0:
                config[policy] = None
        else:
            config[policy] = None


def _validate_policy(policy_value):
    """Validate policy value.
    Behaviour is similar in all three policies: time, size, count,
    that is why one validation is suitable for all.

    Args:
        policy_value (int or NoneType): value of a policy.

    Raises:
        ValueError:
            - policy value is not an integer.
            - policy valie is invalid.

    """
    if policy_value is None:
        return

    if not isinstance(policy_value, int):
        raise ValueError('policy value is not an integer')

    if policy_value < 0:
        raise ValueError('policy value is not valid')
