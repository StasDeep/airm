#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""Argparsers for three main entry points.
This module uses argparse module to parse command line arguments.
The purpose of this module is to convert command line arguments into variables,
that are passed to library-level functions.

"""

import argparse
import os

import airm._constants as constants

def config_args():
    """Parse arguments in 'airconfig' entry point.

    Returns:
        argparse.Namespace: collection of passed arguments.

    """
    parser_config = argparse.ArgumentParser(
        description='Change or view AIRM options.')

    parser_config.add_argument(
        '-s', '--show',
        action='store_true',
        help='show config in Human Readable Format')
    parser_config.add_argument(
        '--trash-dir',
        metavar='DIRECTORY',
        default=None,
        help='directory, where trashbin is stored')
    parser_config.add_argument(
        '--time-policy',
        metavar='DAYS',
        nargs='?',
        type=int,
        const=0,
        help='how many days files are stored in trashbin. '
             'If passed without DAYS or with zero, disable policy')
    parser_config.add_argument(
        '--size-policy',
        metavar='MB',
        nargs='?',
        type=int,
        const=0,
        help='maximum trashbin size in megabytes. '
             'If passed without MB or with zero, disable policy')
    parser_config.add_argument(
        '--count-policy',
        metavar='COUNT',
        nargs='?',
        type=int,
        const=0,
        help='maximum files amount in trashbin. '
             'If passed without COUNT or with zero, disable policy')
    parser_config.add_argument(
        '--config-file',
        metavar='CONFIG',
        default=constants.CONFIG_PATH,
        help='run with definite parameters written in CONFIG')
    parser_config.add_argument(
        '--silent',
        action='store_true',
        dest='silent_run',
        help='run without logging')

    args = parser_config.parse_args()
    args.config_file = os.path.abspath(args.config_file)

    return args

def rm_args():
    """Parse arguments in 'airm' entry point.

    Returns:
        argparse.Namespace: collection of passed arguments.

    """
    parser_rm = argparse.ArgumentParser(
        description='Remove files and directories.')

    parser_rm.add_argument(
        '-d', '--dir',
        action='store_true',
        dest='remove_empty_dir',
        help='remove empty directories')
    parser_rm.add_argument(
        '-f', '--force',
        action='store_true',
        help='ignore warnings and prompts')
    parser_rm.add_argument(
        '-i', '--interactive',
        action='store_true',
        dest='interactive_run',
        help='prompt before every removal')
    parser_rm.add_argument(
        '-r', '--recursive',
        action='store_true',
        dest='remove_recursively',
        help='remove directories')
    parser_rm.add_argument(
        '--silent',
        action='store_true',
        dest='silent_run',
        help='run without logging, --force flag is added automatically')
    parser_rm.add_argument(
        '--dry-run',
        action='store_true',
        help='run without making any influence')
    parser_rm.add_argument(
        '--config-file',
        metavar='CONFIG',
        default=constants.CONFIG_PATH,
        help='run with definite parameters written in CONFIG')
    parser_rm.add_argument(
        '--trash-dir',
        metavar='DIRECTORY',
        default=None,
        help='directory, where trashbin is stored')
    parser_rm.add_argument(
        '--time-policy',
        metavar='DAYS',
        type=int,
        default=None,
        help='how many days files are stored in trashbin. '
             'If passed without DAYS or with zero, disable policy')
    parser_rm.add_argument(
        '--size-policy',
        metavar='MB',
        type=int,
        default=None,
        help='maximum trashbin size in megabytes. '
             'If passed without MB or with zero, disable policy')
    parser_rm.add_argument(
        '--count-policy',
        metavar='COUNT',
        type=int,
        default=None,
        help='maximum files amount in trashbin. '
             'If passed without COUNT or with zero, disable policy')
    parser_rm.add_argument(
        '--multi',
        action='store_true',
        help='use multiprocessing for removal')

    # Forbid passing files and regexp simultaneously.
    files_regexp_group = parser_rm.add_mutually_exclusive_group(required=True)
    files_regexp_group.add_argument(
        'files',
        metavar='FILE',
        nargs='*',
        default=[],
        help='file to be deleted')
    files_regexp_group.add_argument(
        '--regexp',
        metavar=('PATTERN', 'DIRECTORY'),
        nargs=2,
        help='remove files and directories in a DIRECTORY by PATTERN')

    args = parser_rm.parse_args()
    args.config_file = os.path.abspath(args.config_file)

    # Silence means not only absense of output,
    # but also absence of questions.
    if args.silent_run:
        args.force = True

    return args

def trash_args():
    """
    Parse arguments in 'airtrash' entry point.

    Returns:
        argparse.Namespace: collection of passed arguments.

    """
    parser_trash = argparse.ArgumentParser(
        description='Manipulate trashbin files removed with AIRM.')
    parser_trash.add_argument(
        '-s', '--show',
        action='store_true',
        help='show trashbin contents')
    parser_trash.add_argument(
        '-c', '--clean',
        metavar='FILE',
        nargs='*',
        help='clean trashbin contents. If no files passed, clean everything')
    parser_trash.add_argument(
        '-r', '--recover',
        metavar='FILE',
        nargs='*',
        help='recover trashbin contents. '
             'If no files passed, recover everything')
    parser_trash.add_argument(
        '-d', '--create-dir',
        action='store_true',
        help='if original directory does not exist while recovery, create new')
    parser_trash.add_argument(
        '-a', '--all',
        action='store_true',
        dest='take_all',
        help='if there are several files in trashbin with identical names, take all of them')
    parser_trash.add_argument(
        '-i', '--interactive',
        action='store_true',
        dest='interactive_run',
        help='prompt before every removal/recovery')
    parser_trash.add_argument(
        '--silent',
        action='store_true',
        dest='silent_run',
        help='run without logging, '
             '--all, --create-dir and --suffix flags are added automatically')
    parser_trash.add_argument(
        '--dry-run',
        action='store_true',
        help='run without making any influence')
    parser_trash.add_argument(
        '--config-file',
        metavar='CONFIG',
        default=constants.CONFIG_PATH,
        help='file with definite parameters')
    parser_trash.add_argument(
        '--trash-dir',
        metavar='DIRECTORY',
        default=None,
        help='directory, where trashbin is stored')
    parser_trash.add_argument(
        '--time-policy',
        metavar='DAYS',
        type=int,
        default=None,
        help='how many days files are stored in trashbin. '
             'If passed without DAYS or with zero, disable policy')
    parser_trash.add_argument(
        '--size-policy',
        metavar='MB',
        type=int,
        default=None,
        help='maximum trashbin size in megabytes. '
             'If passed without MB or with zero, disable policy')
    parser_trash.add_argument(
        '--count-policy',
        metavar='COUNT',
        type=int,
        default=None,
        help='maximum files amount in trashbin. '
             'If passed without COUNT or with zero, disable policy')

    # Solve name conflict without prompting.
    names_conflict_group = parser_trash.add_mutually_exclusive_group()
    names_conflict_group.add_argument(
        '--suffix',
        action='store_true',
        help='add suffix (n), if file already exists while recovery')
    names_conflict_group.add_argument(
        '--replace',
        action='store_true',
        help='replace file, if it already exists while recovery')

    args = parser_trash.parse_args()
    args.config_file = os.path.abspath(args.config_file)

    # Silence means not only absense of output,
    # but also absence of questions.
    if args.silent_run:
        args.take_all = True
        args.create_dir = True
        if not args.replace:
            args.suffix = True

    return args
