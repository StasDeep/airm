#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""AIRM utilities.
Utilites for other modules in the package.
Contains mostly functions to work with files.

"""

import datetime
import os


def move(old_path, new_path):
    """Move file or directory with replacing.

    Args:
        old_path (str): path to move from.
        new_path (str): path to move to.

    """
    if not os.path.exists(old_path):
        raise OSError('path does not exist')

    if os.path.exists(new_path):
        remove(new_path)
    os.rename(old_path, new_path)


def remove(path):
    """Remove file or directory.

    Args:
        path (str): path to remove.

    """
    if not os.path.exists(path):
        raise OSError('path does not exist')

    if os.path.isdir(path):
        remove_dir(path)
    else:
        os.remove(path)


def remove_dir(path):
    """Recursively remove directory.

    Args:
        path (str): path of directory to remove.

    """
    for dirpath, _, filenames in os.walk(path, topdown=False):
        for filename in filenames:
            os.remove(os.path.join(dirpath, filename))
        os.rmdir(dirpath)


def get_file_info(path):
    """Return filedict.

    Args:
        path (str): path to a file to get information about.

    Returns:
        dict: dictionary with next attributes:
            - 'name' - file name (without path).
            - 'type' - file type ('dir' or 'file').
            - 'time' - time of deletion (current time).
            - 'size' - file size (in bytes).
            - 'dir'  - original file location.
    """
    filedict = dict()

    filedict['name'] = os.path.basename(path)
    filedict['type'] = file_or_dir(path)
    filedict['time'] = str(datetime.datetime.now())
    try:
        filedict['size'] = get_file_size(path)
    except OSError:
        filedict['size'] = 0
    filedict['dir'] = os.path.dirname(path)
    return filedict


def get_file_size(path):
    """Recursively calculate file or directory size.

    Args:
        path (str): path to file to get size of.

    """
    size = os.path.getsize(path)

    if os.path.isdir(path):
        for name in os.listdir(path):
            new_path = os.path.join(path, name)
            plus_size = get_file_size(new_path)
            size += plus_size

    return size


def file_or_dir(path):
    """Return type of file object.

    Args:
        path (str): path to get type of.

    Returns:
        str:
            'file': if file object is a file.
            'dir': if file object is a directory.

    """
    if os.path.isdir(path):
        return 'dir'
    else:
        return 'file'


def yes_no_prompt(message):
    """Ask a yes/no question and return answer.

    Args:
        message (str): question to ask.

    Return:
        bool: True if 'yes' or 'y' is entered
    """
    ans = raw_input(message + ' (Y/n) ')
    if ans.lower() == 'y' or ans.lower() == 'yes':
        return True
    return False


def is_writable(path):
    """Check if file/directory is writable.
    Directory is also not writable, if there is any file or directory inside
    that is not writable.

    Args:
        path (str): path to check writability.

    Return:
        bool: True, if is writable.

    """
    if os.path.isdir(path):
        for dirpath, _, filenames in os.walk(path):
            for filename in filenames:
                full_path = os.path.join(dirpath, filename)
                if not os.access(full_path, os.W_OK):
                    return False
            if not os.access(dirpath, os.W_OK):
                return False
    else:
        if not os.access(path, os.W_OK):
            return False
    return True


def is_readable(path):
    """Check if file/directory is readable.
    Directory is also not readable, if there is any file or directory inside
    that is not readable.

    Args:
        path (str): path to check readability.

    Return:
        bool: True, if is readable.

    """
    if os.path.isdir(path):
        for dirpath, _, filenames in os.walk(path):
            for filename in filenames:
                full_path = os.path.join(dirpath, filename)
                if not os.access(full_path, os.R_OK):
                    return False
            if not os.access(dirpath, os.R_OK):
                return False
    else:
        if not os.access(path, os.R_OK):
            return False
    return True


def extend_dict(dest, **kwargs):
    """Add fields to dict.
    If keyword argument is not None, extend dictionary with its value.

    Args:
        dest (dict): dictionary to be extended.
        **kwargs: key-value pairs to be added.

    """
    for key in kwargs:
        if kwargs[key] is not None:
            dest[key] = kwargs[key]


def resulting_dict(result, policy_files=None):
    """Fill dict with information about result.

    Args:
        result (list): list of dicts with 'msg' key.
        result (str): error message.

    """
    if isinstance(result, (list, tuple)):
        is_erroneous = any(error_dict['msg'] for error_dict in result)
        return {
            'is_erroneous': is_erroneous,
            'type': 'list',
            'items': result,
            'policy_files': policy_files
        }
    elif isinstance(result, basestring):
        is_erroneous = result != ''
        return {
            'is_erroneous': is_erroneous,
            'type': 'str',
            'items': result,
            'policy_files': []
        }
