#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""AIRM remove.
Rm Module is made for moving files to trashbin.

"""

import multiprocessing
import logging
import os
import functools
import re

import airm.util as util
import airm.config as config
import airm._constants as constants
import airm._logger as logger
import airm._trash_base as trash_base


def remove(
        filenames,
        remove_empty_dir=True,
        remove_recursively=True,
        force=True,
        interactive_run=False,
        dry_run=False,
        silent_run=True,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_path=constants.CONFIG_PATH,
        multiprocess=False):
    """Remove files and directories.

    Args:
        filenames (list): list with names of files to be removed.
        remove_empty_dir (bool): if True, remove empty directories.
        remove_recursively (bool): if True, remove directories.
        force (bool): if True, don't ask to remove write-protected file.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        silent_run (bool): if True, run without output.
        trash_dir (str): path of trashbin.
        time_policy (int): number of days after that files are removed.
        size_policy (int): maximum size of trashbin in megabytes.
        count_policy (int): maximum amount of files in trashbin.
        config_path (str): path of config file.

    Raises:
        TypeError: if filenames are not in list or tuple.
        ValueError: if custom config does not exist or invalid.

    Returns:
        dict: list of dicts with filename and error message.

    """
    if isinstance(filenames, str):
        raise TypeError('filenames must be list or tuple')

    if silent_run:
        logger.set_silent()

    if dry_run:
        logger.set_dry()
        logging.warning('running in dry mode, nothing will be changed')

    config_dict = config.load_config(config_path)

    util.extend_dict(config_dict,
                     trash_path=trash_dir,
                     time_policy=time_policy,
                     size_policy=size_policy,
                     count_policy=count_policy)

    trash_path = config_dict['trash_path']

    try:
        filedicts = trash_base.load_trash_base(trash_path)
    except OSError, IOError:
        return util.resulting_dict('trash_path is invalid')

    curried_remove = functools.partial(
        _remove_file,
        remove_empty_dir=remove_empty_dir,
        remove_recursively=remove_recursively,
        force=force,
        interactive_run=interactive_run,
        dry_run=dry_run,
        config_path=config_path,
        trash_path=trash_path
    )

    if multiprocess:
        pool = multiprocessing.Pool()
        try:
            result = pool.map(curried_remove, filenames)
        finally:
            pool.close()
            pool.join()
    else:
        result = []
        for filename in filenames:
            filedict = curried_remove(filename)
            result.append(filedict)

    result = [filedict for filedict in result if filedict is not None]

    trash_base.extend_trash(filedicts, result)
    policy_files = trash_base.apply_policies(config_dict, filedicts)
    trash_base.write_trash_base(trash_path, filedicts)

    return util.resulting_dict(result, policy_files)


def remove_by_pattern(
        pattern,
        directory,
        remove_empty_dir=True,
        remove_recursively=True,
        force=True,
        interactive_run=False,
        dry_run=False,
        silent_run=True,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_path=constants.CONFIG_PATH):
    """Remove files and directories by pattern in a specific directory.

    Args:
        pattern (str): regular expression as string.
        directory (str): path where files are to be deleted.
        remove_empty_dir (bool): if True, remove empty directories.
        remove_recursively (bool): if True, remove directories.
        force (bool): if True, don't ask to remove write-protected file.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        silent_run (bool): if True, run without output.
        trash_dir (str): path of trashbin.
        time_policy (int): number of days after that files are removed.
        size_policy (int): maximum size of trashbin in megabytes.
        count_policy (int): maximum amount of files in trashbin.
        config_path (str): path of config file.
        multiprocess (bool): use multiprocessing for more fast removal
            of big amount of files.

    Raises:
        ValueError: if custom config does not exist or invalid.
        RecursiveRemovalError:
            - if regular expression is invalid.
            - if directory does not exist.

    Returns:
        dict: list of dicts with filename and error message.

    """
    if silent_run:
        logger.set_silent()

    if dry_run:
        logger.set_dry()
        logging.warning('running in dry mode, nothing will be changed')

    config_dict = config.load_config(config_path)

    util.extend_dict(config_dict,
                     trash_path=trash_dir,
                     time_policy=time_policy,
                     size_policy=size_policy,
                     count_policy=count_policy)

    trash_path = config_dict['trash_path']

    try:
        filedicts = trash_base.load_trash_base(trash_path)
    except OSError, IOError:
        return util.resulting_dict('trash_path is invalid')

    try:
        regexp = re.compile(pattern)
    except re.error:
        return util.resulting_dict('invalid regular expression')

    if not os.path.exists(directory) or not os.path.isdir(directory):
        return util.resulting_dict(
            '\'{}\' does not exist or is not a directory'.format(directory))

    result = []
    for dirpath, dirnames, filenames in os.walk(directory, topdown=False):
        dirpath = os.path.abspath(dirpath)
        for path in filenames + dirnames:
            if regexp.search(path):
                full_path = os.path.join(dirpath, path)
                filedict = _remove_file(
                    full_path,
                    remove_empty_dir,
                    remove_recursively,
                    force,
                    interactive_run,
                    dry_run,
                    config_path,
                    trash_path
                )
                result.append(filedict)

    result = [filedict for filedict in result if filedict is not None]

    trash_base.extend_trash(filedicts, result)
    policy_files = trash_base.apply_policies(config_dict, filedicts)
    trash_base.write_trash_base(trash_path, filedicts)

    return util.resulting_dict(result, policy_files)


def _remove_file(
        path,
        remove_empty_dir,
        remove_recursively,
        force,
        interactive_run,
        dry_run,
        config_path,
        trash_path):
    """Remove file or directory by path.

    Args:
        path (str): path of a file to be removed.
        remove_empty_dir (bool): if True, remove empty directories.
        remove_recursively (bool): if True, remove directories.
        force (bool): if True, don't ask to remove write-protected file.
        interactive_run (bool): if True, ask permission before every removal.
        dry_run (bool): if True, run without making any changes.
        silent_run (bool): if True, run without output.
        config_path (str): path of config file.
        trash_path (str): path of trashbin directory.

    Returns:
        str: timestamp when file was deleted or None.

    """
    path = os.path.abspath(path)
    filedict = util.get_file_info(path)
    filedict['msg'] = ''

    try:
        _check_if_removable(
            path, remove_empty_dir, remove_recursively,
            force, config_path, trash_path)
    except Warning as warning:
        return None
    except OSError as error:
        filedict['msg'] = str(error)
        return filedict

    filetype = 'directory' if os.path.isdir(path) else 'file'
    prompt = 'Remove {} \'{}\'?'.format(filetype, path)
    if not interactive_run or util.yes_no_prompt(prompt):
        if not dry_run:
            filedict['time'] = trash_base.move_file_to_trash(path, trash_path)

        logging.info('moving \'{}\' to trashbin'.format(path))
        if dry_run:
            return None
    else:
        return None

    return filedict


def _check_if_removable(
        path,
        remove_empty_dir,
        remove_recursively,
        force,
        config_path,
        trash_path):
    """Raise exception if file/directory cannot be removed.

    Raises:
        OSError:
            - path does not exist.
            - path is a config file.
            - path is inside trashbin.
            - path is an empty directory and
                remove_empty_dir and remove_recursively are both False.
            - path is a non-empty directory and
                remove_recursively is False.
        Warning:
            - if force is False and user answers 'no'.
    """
    if not os.path.exists(path):
        raise OSError('No such file/directory: {}.'.format(path))

    if path == config_path:
        raise OSError('cannot remove config file')

    if os.path.dirname(trash_path).startswith(path) or path.startswith(trash_path):
        raise OSError('cannot remove trash components')

    is_dir = os.path.isdir(path)
    rm_only_files = not remove_empty_dir and not remove_recursively
    if is_dir and rm_only_files:
        raise OSError('{} is a directory. Cannot remove it'.format(path))

    not_recursive = not remove_recursively
    not_empty_dir = False

    # If os.listdir raises exception, than there are no read rights.
    try:
        if is_dir:
            not_empty_dir = bool(os.listdir(path))
    except OSError:
        raise OSError('directory {} has no read rights. Cannot remove it'\
            .format(path))

    if not_empty_dir and not_recursive:
        raise OSError('directory {} is not empty. Cannot remove it'\
            .format(path))

    writable = util.is_writable(path)
    if not writable and not force:
        prompt = ''
        if os.path.isdir(path):
            prompt = 'Directory {} contains write-protected files.'\
                     'Remove them?'.format(path)
        else:
            prompt = 'Remove write-protected file {}?'.format('path')
        if not util.yes_no_prompt(prompt):
            raise Warning('skipping \'{}\''.format(path))
