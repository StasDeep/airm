#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""AIRM.
This package is used for more secure removal.
It uses trashbin to recover removed files.

Examples:
    >>> airm.rm.remove(['file_1', 'file_2'])
    >>> airm.trash.show()
    Total: 2
    1. file_1  file  0  2017-04-15 12:44:29
    2. file_2  file  0  2017-04-15 12:44:29
    >>> airm.trash.clean(['file1'])
    >>> airm.trash.recover(['file2'])

"""

import airm._logger as _logger
import airm.config as config
import airm.hrf as hrf
import airm.rm as rm
import airm.trash as trash
import airm.util as util

_logger.set_logger()

__all__ = ['config', 'hrf', 'rm', 'trash', 'util']
