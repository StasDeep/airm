#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""Handlers that are called when using AIRM as bash command.
Public module wrappers to correctly use command line arguments.

"""

import logging

import airm._argparsers as argparsers
import airm.config as config
import airm._constants as constants
import airm.rm as rm
import airm.trash as trash

def config_handler(args=None):
    """Run airm.config module according to arguments.

    Args:
        args (argparse.Namespace): command line arguments.
            args is None, if config_handler is called as an entry point.
            args is not None, if handler is called as a function (in tests).

    Returns:
        int: exit code of the function.

    """
    if args is None:
        args = argparsers.config_args()

    args_not_passed = (args.trash_dir is None and
                       args.time_policy is None and
                       args.size_policy is None and
                       args.count_policy is None)

    # --show
    if args_not_passed or args.show:
        try:
            config.show(args.config_file)
        except (OSError, ValueError) as error:
            logging.error(error)
            return constants.CONFIG_FAIL_CODE

    # --trash-folder
    if args.trash_dir is not None:
        try:
            config.move_trash(
                new_trash_path=args.trash_dir,
                config_path=args.config_file,
                silent_run=args.silent_run)
        except (OSError, ValueError) as error:
            logging.error(error)
            return constants.CONFIG_FAIL_CODE

    # --time-policy
    if args.time_policy is not None:
        try:
            config.set_time_policy(
                value=args.time_policy,
                config_path=args.config_file,
                silent_run=args.silent_run)
        except (OSError, ValueError) as error:
            logging.error(error)
            return constants.CONFIG_FAIL_CODE

    # --size-policy
    if args.size_policy is not None:
        try:
            config.set_size_policy(
                value=args.size_policy,
                config_path=args.config_file,
                silent_run=args.silent_run)
        except (OSError, ValueError) as error:
            logging.error(error)
            return constants.CONFIG_FAIL_CODE

    # --count-policy
    if args.count_policy is not None:
        try:
            config.set_count_policy(
                value=args.count_policy,
                config_path=args.config_file,
                silent_run=args.silent_run)
        except (OSError, ValueError) as error:
            logging.error(error)
            return constants.CONFIG_FAIL_CODE

    return constants.SUCCESS_CODE


def rm_handler(args=None):
    """Run airm.rm module according to arguments.

    Args:
        args (argparse.Namespace): command line arguments.
            args is None, if config_handler is called as an entry point.
            args is not None, if handler is called as a function (in tests).

    Returns:
        int: exit code of the function.

    """
    if args is None:
        args = argparsers.rm_args()

    if args.files:
        try:
           result = rm.remove(
                filenames=args.files,
                remove_empty_dir=args.remove_empty_dir,
                remove_recursively=args.remove_recursively,
                force=args.force,
                interactive_run=args.interactive_run,
                dry_run=args.dry_run,
                silent_run=args.silent_run,
                config_path=args.config_file,
                trash_dir=args.trash_dir,
                time_policy=args.time_policy,
                size_policy=args.size_policy,
                count_policy=args.count_policy,
                multiprocess=args.multi)
        except ValueError as error:
            logging.error(error)
            return constants.RM_FAIL_CODE

        if result['is_erroneous']:
            logging.error(_error_msg(result, 'remove'))
            return constants.RM_FAIL_CODE


    if args.regexp:
        try:
            result = rm.remove_by_pattern(
                pattern=args.regexp[0],
                directory=args.regexp[1],
                remove_empty_dir=args.remove_empty_dir,
                remove_recursively=args.remove_recursively,
                force=args.force,
                interactive_run=args.interactive_run,
                silent_run=args.silent_run,
                config_path=args.config_file,
                trash_dir=args.trash_dir,
                time_policy=args.time_policy,
                size_policy=args.size_policy,
                count_policy=args.count_policy)
        except (ValueError) as error:
            logging.error(error)
            return constants.RM_FAIL_CODE

        if result['is_erroneous']:
            logging.error(_error_msg(result, 'remove'))
            return constants.RM_FAIL_CODE

    return constants.SUCCESS_CODE

def trash_handler(args=None):
    """Run airm.trash module according to arguments.

    Args:
        args (argparse.Namespace): command line arguments.
            args is None, if config_handler is called as an entry point.
            args is not None, if handler is called as a function (in tests).

    Returns:
        int: exit code of the function.

    """
    if args is None:
        args = argparsers.trash_args()

    args_not_passed = args.clean is None and args.recover is None

    if args.show or args_not_passed:
        try:
            trash.show(
                trash_dir=args.trash_dir,
                time_policy=args.time_policy,
                size_policy=args.size_policy,
                count_policy=args.count_policy,
                config_path=args.config_file)
        except (OSError, ValueError) as error:
            logging.error(error)
            return constants.CONFIG_FAIL_CODE

    if args.clean is not None:
        try:
            result = trash.clean(
                filenames=args.clean,
                take_all=args.take_all,
                interactive_run=args.interactive_run,
                dry_run=args.dry_run,
                silent_run=args.silent_run,
                trash_dir=args.trash_dir,
                time_policy=args.time_policy,
                size_policy=args.size_policy,
                count_policy=args.count_policy,
                config_path=args.config_file)
        except (ValueError, TypeError, OSError) as error:
            logging.error(error)
            return constants.CLEAN_FAIL_CODE

        if result['is_erroneous']:
            logging.error(_error_msg(result, 'clean'))
            return constants.CLEAN_FAIL_CODE

    if args.recover is not None:
        try:
            result = trash.recover(
                filenames=args.recover,
                take_all=args.take_all,
                interactive_run=args.interactive_run,
                dry_run=args.dry_run,
                silent_run=args.silent_run,
                create_dir=args.create_dir,
                suffix=args.suffix,
                replace=args.replace,
                trash_dir=args.trash_dir,
                time_policy=args.time_policy,
                size_policy=args.size_policy,
                count_policy=args.count_policy,
                config_path=args.config_file)
        except (ValueError, TypeError, OSError) as error:
            logging.error(error)
            return constants.RECOVER_FAIL_CODE

        if result['is_erroneous']:
            logging.error(_error_msg(result, 'recover'))
            return constants.RECOVER_FAIL_CODE

    return constants.SUCCESS_CODE


def _error_msg(result, action):
    if result['type'] == 'str':
        return result['type']

    errors = [error for error in result['items'] if error['msg'] != '']

    errors_str = '\n'.join('  {}: {}'.format(err['name'], err['msg'])
                  for err in errors)

    return 'cannot {} file(s):\n{}'.format(action, errors_str)
