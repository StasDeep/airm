from setuptools import setup, find_packages
from os.path import join, dirname

PROGRAM_NAME = 'airm'

script = '{name} = {name}._{name}:main'.format(name=PROGRAM_NAME)

setup(
    name=PROGRAM_NAME,
    version='1.0',
    description='Shell command for removing files',
    author='Stas Glubokiy',
    author_email='glubokiy.stas@gmail.com',
    url='https://bitbucket.org/StasDeep/' + PROGRAM_NAME,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
                'airm = airm._cl_handlers:rm_handler',
                'airtrash = airm._cl_handlers:trash_handler',
                'airconfig = airm._cl_handlers:config_handler'
            ]
        }
)
