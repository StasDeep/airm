#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import pytest
import shutil
import StringIO
import sys

import airm
import airm._constants as constants

from tests.constants import (
    FILE_NAMES, FILE_PATHS, TESTING_DIR_PATH, DIR_NAMES, DIR_PATHS,
    CUSTOM_CONFIG, CUSTOM_CONFIG_PATH
)
from tests.helpers import (
    get_trash_filenames, get_trash_filenames_with_config,
    get_trash_filenames_with_trash_path
)
# ===== Setup and Teardown =====

def setup_module(module):
    if os.path.exists(constants.CONFIG_PATH):
        os.remove(constants.CONFIG_PATH)

    if os.path.exists(constants.DEFAULT_TRASH_PATH):
        shutil.rmtree(constants.DEFAULT_TRASH_PATH)

    if not os.path.exists(TESTING_DIR_PATH):
        os.makedirs(TESTING_DIR_PATH)


def teardown_module(module):
    shutil.rmtree(TESTING_DIR_PATH)


def setup_function(function):
    for file_path in FILE_PATHS:
        with open(file_path, 'a'):
            pass

    for dir_path in DIR_PATHS:
        os.makedirs(dir_path)


def teardown_function(function):
    for file_path in os.listdir(TESTING_DIR_PATH):
        full_path = os.path.join(TESTING_DIR_PATH, file_path)
        if os.path.isfile(full_path):
            os.remove(full_path)
        else:
            shutil.rmtree(full_path)
    airm.trash.clean([])


# ===== Fixtures =====

@pytest.fixture
def answer_yes_setup(request):
    yes_no_prompt = airm.rm.util.yes_no_prompt

    def answer_yes_teardown():
        airm.rm.util.yes_no_prompt = yes_no_prompt

    airm.rm.util.yes_no_prompt = lambda msg: True
    request.addfinalizer(answer_yes_teardown)


@pytest.fixture
def answer_no_setup(request):
    yes_no_prompt = airm.rm.util.yes_no_prompt

    def answer_no_teardown():
        airm.rm.util.yes_no_prompt = yes_no_prompt

    airm.rm.util.yes_no_prompt = lambda msg: False
    request.addfinalizer(answer_no_teardown)


@pytest.fixture
def capture_stdout_setup(request):
    from airm._logger import set_logger

    saved_stdout = sys.stdout

    def capture_stdout_teardown():
        sys.stdout = saved_stdout

    out = StringIO.StringIO()
    sys.stdout = out
    set_logger()

    request.addfinalizer(capture_stdout_teardown)

    return out


# ===== Tests =====

def test_simple_remove():
    airm.rm.remove(FILE_PATHS)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_dry_remove():
    airm.rm.remove(FILE_PATHS, dry_run=True)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_remove_by_pattern():
    airm.rm.remove_by_pattern('file_[123]', TESTING_DIR_PATH)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS[3:])
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS[:3])
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES[3:])
    assert all(filename in trash_files for filename in FILE_NAMES[:3])


def test_dry_remove_by_pattern():
    airm.rm.remove_by_pattern('file_[123]', TESTING_DIR_PATH, dry_run=True)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_empty_dir_remove():
    airm.rm.remove([DIR_PATHS[0]])
    assert not os.path.exists(DIR_PATHS[0])
    assert DIR_NAMES[0] in get_trash_filenames()


def test_empty_dir_remove_without_two_flags():
    result = airm.rm.remove([DIR_PATHS[0]],
                            remove_empty_dir=False,
                            remove_recursively=False)
    assert result['is_erroneous']
    assert result['items'][0]['msg'] != ''


def test_empty_dir_remove_without_recursive_flag():
    airm.rm.remove([DIR_PATHS[0]], remove_empty_dir=False)
    assert not os.path.exists(DIR_PATHS[0])
    assert DIR_NAMES[0] in get_trash_filenames()


def test_non_empty_dir_remove_without_recursive_flag():
    open(os.path.join(DIR_PATHS[0], 'file_1'), 'a').close()
    result = airm.rm.remove([DIR_PATHS[0]], remove_recursively=False)
    assert result['is_erroneous']
    assert result['items'][0]['msg'] != ''


def test_non_empty_dir_remove():
    open(os.path.join(DIR_PATHS[0], 'file_1'), 'a').close()
    airm.rm.remove([DIR_PATHS[0]])
    assert not os.path.exists(DIR_PATHS[0])
    assert DIR_NAMES[0] in get_trash_filenames()


def test_force_remove():
    for file_path in FILE_PATHS:
        os.chmod(file_path, 0444)
    airm.rm.remove(FILE_PATHS)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_non_force_remove_with_yes(answer_yes_setup):
    for file_path in FILE_PATHS:
        os.chmod(file_path, 0444)

    airm.rm.remove(FILE_PATHS, force=False)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_non_force_remove_with_no(answer_no_setup):
    for file_path in FILE_PATHS:
        os.chmod(file_path, 0444)

    airm.rm.remove(FILE_PATHS, force=False)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_interactive_remove_with_yes(answer_yes_setup):
    airm.rm.remove(FILE_PATHS, interactive_run=True)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_interactive_remove_with_no(answer_no_setup):
    airm.rm.remove(FILE_PATHS, interactive_run=True)
    assert all(os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_remove_with_custom_config():
    from airm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)

    airm.rm.remove(FILE_PATHS, config_path=CUSTOM_CONFIG_PATH)
    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames_with_config(CUSTOM_CONFIG_PATH)
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_remove_with_edited_config():
    airm.rm.remove(FILE_PATHS, trash_dir=CUSTOM_CONFIG['trash_path'])

    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames_with_trash_path(
        CUSTOM_CONFIG['trash_path'])
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_remove_with_string_as_argument():
    with pytest.raises(TypeError):
        airm.rm.remove(FILE_PATHS[0])


def test_remove_by_pattern_with_invalid_regexp():
    result = airm.rm.remove_by_pattern('file(', TESTING_DIR_PATH)
    assert result['is_erroneous']


def test_remove_by_pattern_with_nonexisting_dir():
    result = airm.rm.remove_by_pattern('file', os.path.join(TESTING_DIR_PATH, 'nodir'))
    assert result['is_erroneous']


def test_remove_by_pattern_with_file_instead_dir():
    result = airm.rm.remove_by_pattern('file', FILE_PATHS[0])
    assert result['is_erroneous']


def test_silent_remove(capture_stdout_setup):
    airm.rm.remove(FILE_PATHS, silent_run=True)
    assert capture_stdout_setup.getvalue() == ''


def test_verbose_remove(capture_stdout_setup):
    airm.rm.remove(FILE_PATHS, silent_run=False)
    assert capture_stdout_setup.getvalue() != ''


def test_remove_with_partial_nonexistence():
    result = airm.rm.remove(FILE_PATHS + ['nonexistent_file'])
    assert result['is_erroneous']
    assert len([error for error in result['items'] if error['msg'] != '']) == 1

    assert all(not os.path.exists(file_path) for file_path in FILE_PATHS)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert 'nonexistent_file' not in trash_files


