#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import pytest
import os
import shutil
import StringIO
import sys

import airm
import airm._constants as constants

from tests.constants import (
    TESTING_DIR_PATH, CUSTOM_CONFIG,
    CUSTOM_CONFIG_PATH, FILE_PATHS
)

# ===== Setup and Teardown =====

def setup_module(module):
    if not os.path.exists(TESTING_DIR_PATH):
        os.makedirs(TESTING_DIR_PATH)


def teardown_module(module):
    shutil.rmtree(TESTING_DIR_PATH)


def setup_function(function):
    if os.path.exists(constants.CONFIG_PATH):
        os.remove(constants.CONFIG_PATH)


# ===== Fixtures =====

@pytest.fixture
def capture_stdout_setup(request):
    from airm._logger import set_logger

    saved_stdout = sys.stdout

    def capture_stdout_teardown():
        sys.stdout = saved_stdout

    out = StringIO.StringIO()
    sys.stdout = out
    set_logger()

    request.addfinalizer(capture_stdout_teardown)

    return out


# ===== Tests =====

def test_move_trash():
    airm.config.move_trash(CUSTOM_CONFIG['trash_path'])
    assert os.path.exists(CUSTOM_CONFIG['trash_path'])
    assert not os.path.exists(constants.DEFAULT_TRASH_PATH)
    assert airm.config.get_trash_path() == CUSTOM_CONFIG['trash_path']


def test_set_policies():
    airm.config.set_time_policy(CUSTOM_CONFIG['time_policy'])
    airm.config.set_size_policy(CUSTOM_CONFIG['size_policy'])
    airm.config.set_count_policy(CUSTOM_CONFIG['count_policy'])

    new_policies = airm.config.get_policies()

    for policy in constants.POLICIES:
        assert new_policies[policy] == CUSTOM_CONFIG[policy]


def test_set_zero_valiues_for_policies():
    airm.config.set_time_policy(0)
    airm.config.set_size_policy(0)
    airm.config.set_count_policy(0)

    new_policies = airm.config.get_policies()

    for policy in constants.POLICIES:
        assert new_policies[policy] is None


def test_custom_config():
    from airm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)

    airm.config.set_time_policy(CUSTOM_CONFIG['time_policy'] + 1,
                                CUSTOM_CONFIG_PATH)
    airm.config.set_size_policy(CUSTOM_CONFIG['size_policy'] + 1,
                                CUSTOM_CONFIG_PATH)
    airm.config.set_count_policy(CUSTOM_CONFIG['count_policy'] + 1,
                                 CUSTOM_CONFIG_PATH)

    new_policies = airm.config.get_policies(CUSTOM_CONFIG_PATH)

    for policy in constants.POLICIES:
        assert new_policies[policy] == CUSTOM_CONFIG[policy] + 1


def test_silent_config_change(capture_stdout_setup):
    airm.config.set_time_policy(CUSTOM_CONFIG['time_policy'])
    airm.config.set_size_policy(CUSTOM_CONFIG['size_policy'])
    airm.config.set_count_policy(CUSTOM_CONFIG['count_policy'])

    assert capture_stdout_setup.getvalue() == ''


def test_verbose_config_change(capture_stdout_setup):
    airm.config.set_time_policy(CUSTOM_CONFIG['time_policy'],
                                silent_run=False)
    airm.config.set_size_policy(CUSTOM_CONFIG['size_policy'],
                                silent_run=False)
    airm.config.set_count_policy(CUSTOM_CONFIG['count_policy'],
                                 silent_run=False)

    assert capture_stdout_setup.getvalue() != ''


# ===== Exception Tests =====

def test_original_trash_does_not_exist():
    trash_path = airm.config.get_trash_path()
    if os.path.exists(trash_path):
        shutil.rmtree(trash_path)

    with pytest.raises(OSError):
        airm.config.move_trash(CUSTOM_CONFIG['trash_path'])


def test_move_trash_to_the_same_dir():
    airm.trash.get_trash_files()

    with pytest.raises(OSError):
        airm.config.move_trash(airm.config.get_trash_path())


def test_move_trash_to_nonempty_dir():
    airm.trash.get_trash_files()

    # Make testing directory nonempty.
    with open(FILE_PATHS[0], 'a'):
        pass

    # Try moving trash to nonempty directory.
    with pytest.raises(OSError):
        airm.config.move_trash(TESTING_DIR_PATH)


def test_move_to_dir_with_nonexisting_parent():
    with pytest.raises(OSError):
        airm.config.move_trash(os.path.join(TESTING_DIR_PATH, 'dir1', 'dir2'))


def test_loading_nonexisting_config():
    with pytest.raises(OSError):
        airm.config.get_trash_path(CUSTOM_CONFIG_PATH + ' (1)')


def test_loading_dir_as_config():
    with pytest.raises(OSError):
        airm.config.get_trash_path(TESTING_DIR_PATH)


def test_loading_invalid_config():
    with open(FILE_PATHS[0], 'a'):
        pass

    with pytest.raises(ValueError):
        airm.config.get_trash_path(FILE_PATHS[0])
