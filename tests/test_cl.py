#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import os
import pytest
import shutil
import StringIO
import sys

import airm
from airm._cl_handlers import rm_handler, config_handler, trash_handler
import airm._constants as constants

from tests.constants import (
    FILE_NAMES, FILE_PATHS, TESTING_DIR_PATH, DIR_NAMES, DIR_PATHS,
    CUSTOM_CONFIG, CUSTOM_CONFIG_PATH
)
from tests.helpers import (
    get_trash_filenames, get_trash_filenames_with_config,
    get_trash_filenames_with_trash_path
)

# ===== Setup and Teardown =====

def setup_module(module):
    if os.path.exists(constants.CONFIG_PATH):
        os.remove(constants.CONFIG_PATH)

    if os.path.exists(constants.DEFAULT_TRASH_PATH):
        shutil.rmtree(constants.DEFAULT_TRASH_PATH)

    if not os.path.exists(TESTING_DIR_PATH):
        os.makedirs(TESTING_DIR_PATH)

    os.chdir(TESTING_DIR_PATH)


def teardown_module(module):
    os.chdir(os.path.dirname(TESTING_DIR_PATH))
    shutil.rmtree(TESTING_DIR_PATH)


def setup_function(function):
    if os.path.exists(constants.CONFIG_PATH):
        os.remove(constants.CONFIG_PATH)

    for file_path in FILE_PATHS:
        with open(file_path, 'a'):
            pass

    for dir_path in DIR_PATHS:
        os.makedirs(dir_path)


def teardown_function(function):
    for file_path in os.listdir(TESTING_DIR_PATH):
        full_path = os.path.join(TESTING_DIR_PATH, file_path)
        if os.path.isfile(full_path):
            os.remove(full_path)
        else:
            shutil.rmtree(full_path)
    airm.trash.clean([])


# ===== Tests 'airm' =====

def test_cl_remove():
    args = argparse.Namespace(
        files=FILE_NAMES,
        regexp=None,
        remove_empty_dir=False,
        remove_recursively=False,
        force=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH,
        multi=False
    )

    assert all(os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = rm_handler(args)
    assert exit_code == constants.SUCCESS_CODE
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)


def test_cl_remove_by_pattern():
    args = argparse.Namespace(
        files=None,
        regexp=('file_\d', '.'),
        remove_empty_dir=False,
        remove_recursively=False,
        force=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH,
        multi=False
    )

    exit_code = rm_handler(args)
    assert exit_code == constants.SUCCESS_CODE
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)


def test_cl_remove_dirs_without_flag():
    args = argparse.Namespace(
        files=DIR_NAMES,
        regexp=None,
        remove_empty_dir=False,
        remove_recursively=False,
        force=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH,
        multi=False
    )

    exit_code = rm_handler(args)
    assert exit_code == constants.RM_FAIL_CODE
    assert all(os.path.exists(dirname) for dirname in DIR_NAMES)


def test_cl_remove_dirs_with_flag():
    args = argparse.Namespace(
        files=DIR_NAMES,
        regexp=None,
        remove_empty_dir=True,
        remove_recursively=False,
        force=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH,
        multi=False
    )

    exit_code = rm_handler(args)
    assert exit_code == constants.SUCCESS_CODE
    assert all(not os.path.exists(dirname) for dirname in DIR_NAMES)


def test_cl_dry_remove():
    args = argparse.Namespace(
        files=FILE_NAMES,
        regexp=None,
        remove_empty_dir=False,
        remove_recursively=False,
        force=False,
        interactive_run=False,
        dry_run=True,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH,
        multi=False
    )

    exit_code = rm_handler(args)
    assert exit_code == constants.SUCCESS_CODE
    assert all(os.path.exists(filename) for filename in FILE_NAMES)


# ===== Tests 'airtrash' =====

def test_cl_clean():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=FILE_NAMES,
        recover=None,
        show=False,
        take_all=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.SUCCESS_CODE

    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_cl_recover():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=None,
        recover=FILE_NAMES,
        show=False,
        take_all=False,
        create_dir=False,
        suffix=False,
        replace=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.SUCCESS_CODE

    trash_files = get_trash_filenames()
    assert all(os.path.exists(filename) for filename in FILE_NAMES)
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_cl_dry_clean():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=FILE_NAMES,
        recover=None,
        show=False,
        take_all=False,
        interactive_run=False,
        dry_run=True,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.SUCCESS_CODE

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_cl_dry_recover():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=None,
        recover=FILE_NAMES,
        show=False,
        take_all=False,
        create_dir=False,
        suffix=False,
        replace=False,
        interactive_run=False,
        dry_run=True,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.SUCCESS_CODE

    trash_files = get_trash_filenames()
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_cl_show_with_nonexisting_config():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=None,
        recover=None,
        show=True,
        take_all=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH + ' (1)'
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.CONFIG_FAIL_CODE

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_cl_clean_with_nonexisting_config():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=FILE_NAMES,
        recover=None,
        show=False,
        take_all=False,
        interactive_run=False,
        dry_run=False,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH + ' (1)'
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.CLEAN_FAIL_CODE

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_cl_recover_with_nonexisting_config():
    airm.rm.remove(FILE_NAMES)

    args = argparse.Namespace(
        clean=None,
        recover=FILE_NAMES,
        show=False,
        take_all=False,
        create_dir=False,
        suffix=False,
        replace=False,
        interactive_run=False,
        dry_run=True,
        silent_run=False,
        trash_dir=None,
        time_policy=None,
        size_policy=None,
        count_policy=None,
        config_file=constants.CONFIG_PATH + ' (1)'
    )

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert all(not os.path.exists(filename) for filename in FILE_NAMES)

    exit_code = trash_handler(args)
    assert exit_code == constants.RECOVER_FAIL_CODE

    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


# ===== Tests 'airconfig' =====

def test_cl_set_policies():
    args = argparse.Namespace(
        show=False,
        trash_dir=None,
        time_policy=5,
        size_policy=0,
        count_policy=1000,
        silent_run=False,
        config_file=constants.CONFIG_PATH
    )

    exit_code = config_handler(args)
    assert exit_code == constants.SUCCESS_CODE

    new_policies = airm.config.get_policies()
    assert new_policies == dict(time_policy=5,
                                size_policy=None,
                                count_policy=1000)


def test_cl_set_policies_with_nonexistent_config():
    args = argparse.Namespace(
        show=False,
        trash_dir=None,
        time_policy=5,
        size_policy=0,
        count_policy=1000,
        silent_run=False,
        config_file=constants.CONFIG_PATH + ' (1)'
    )

    exit_code = config_handler(args)
    assert exit_code == constants.CONFIG_FAIL_CODE

    new_policies = airm.config.get_policies()
    assert new_policies == dict(
        time_policy=constants.DEFAULT_CONFIG['time_policy'],
        size_policy=constants.DEFAULT_CONFIG['size_policy'],
        count_policy=constants.DEFAULT_CONFIG['count_policy']
    )
