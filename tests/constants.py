#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os

TESTING_DIR_NAME = '.airm_testing_dir'
TESTING_DIR_PATH = os.path.join(os.getcwd(), TESTING_DIR_NAME)

FILE_NAMES = ['file_' + str(index) for index in range(1, 6)]
FILE_PATHS = [os.path.join(TESTING_DIR_PATH, FILE_NAME)
              for FILE_NAME in FILE_NAMES]

DIR_NAMES = ['dir_' + str(index) for index in range(1, 6)]
DIR_PATHS = [os.path.join(TESTING_DIR_PATH, DIR_NAME)
              for DIR_NAME in DIR_NAMES]

CUSTOM_TRASH_PATH = os.path.join(TESTING_DIR_PATH, 'test_trash')
CUSTOM_CONFIG_PATH = os.path.join(TESTING_DIR_PATH, 'test_config')

CUSTOM_CONFIG = dict(
    trash_path=CUSTOM_TRASH_PATH,
    time_policy=10,
    size_policy=512,
    count_policy=10000
)
