#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import pytest
import os
import shutil
import StringIO
import sys

import airm
import airm._constants as constants

from tests.constants import (
    FILE_NAMES, FILE_PATHS, TESTING_DIR_PATH,
    DIR_PATHS, CUSTOM_CONFIG, CUSTOM_CONFIG_PATH
)
from tests.helpers import (
    get_trash_filenames, get_trash_filenames_with_config,
    get_trash_filenames_with_trash_path
)


# ===== Setup and Teardown =====

def setup_module(module):
    if os.path.exists(constants.CONFIG_PATH):
        os.remove(constants.CONFIG_PATH)

    if os.path.exists(constants.DEFAULT_TRASH_PATH):
        shutil.rmtree(constants.DEFAULT_TRASH_PATH)

    if not os.path.exists(TESTING_DIR_PATH):
        os.makedirs(TESTING_DIR_PATH)


def teardown_module(module):
    shutil.rmtree(TESTING_DIR_PATH)


def setup_function(function):
    for filepath in FILE_PATHS:
        with open(filepath, 'a'):
            pass
    airm.rm.remove(FILE_PATHS)


def teardown_function(function):
    for file_path in os.listdir(TESTING_DIR_PATH):
        full_path = os.path.join(TESTING_DIR_PATH, file_path)
        if os.path.isfile(full_path):
            os.remove(full_path)
        else:
            shutil.rmtree(full_path)
    airm.trash.clean([])

    if os.path.exists(constants.CONFIG_PATH):
        os.remove(constants.CONFIG_PATH)


# ===== Fixtures =====

@pytest.fixture
def answer_yes_setup(request):
    yes_no_prompt = airm.rm.util.yes_no_prompt

    def answer_yes_teardown():
        airm.rm.util.yes_no_prompt = yes_no_prompt

    airm.rm.util.yes_no_prompt = lambda msg: True
    request.addfinalizer(answer_yes_teardown)


@pytest.fixture
def answer_no_setup(request):
    yes_no_prompt = airm.rm.util.yes_no_prompt

    def answer_no_teardown():
        airm.rm.util.yes_no_prompt = yes_no_prompt

    airm.rm.util.yes_no_prompt = lambda msg: False
    request.addfinalizer(answer_no_teardown)


@pytest.fixture
def take_all_setup(request):
    prompt_file_choice = airm.trash._prompt_file_choice

    def take_all_teardown():
        airm.trash._prompt_file_choice = prompt_file_choice

    airm.trash._prompt_file_choice = lambda filedicts: filedicts
    request.addfinalizer(take_all_teardown)


@pytest.fixture
def take_first_setup(request):
    prompt_file_choice = airm.trash._prompt_file_choice

    def take_first_teardown():
        airm.trash._prompt_file_choice = prompt_file_choice

    airm.trash._prompt_file_choice = lambda filedicts: [filedicts[0]]
    request.addfinalizer(take_first_teardown)


@pytest.fixture
def take_nothing_setup(request):
    prompt_file_choice = airm.trash._prompt_file_choice

    def take_nothing_teardown():
        airm.trash._prompt_file_choice = prompt_file_choice

    airm.trash._prompt_file_choice = lambda filedicts: []
    request.addfinalizer(take_nothing_teardown)


@pytest.fixture
def capture_stdout_setup(request):
    from airm._logger import set_logger

    saved_stdout = sys.stdout

    def capture_stdout_teardown():
        sys.stdout = saved_stdout

    out = StringIO.StringIO()
    sys.stdout = out
    set_logger()

    request.addfinalizer(capture_stdout_teardown)

    return out


# ===== Tests =====

# ========== Basic Tests ==========

def test_clean():
    airm.trash.clean(FILE_NAMES)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_dry_clean():
    airm.trash.clean(FILE_NAMES, dry_run=True)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_recover():
    airm.trash.recover(FILE_NAMES)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)

    assert all(os.path.exists(filepath) for filepath in FILE_PATHS)


def test_dry_recover():
    airm.trash.recover(FILE_NAMES, dry_run=True)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)

    assert all(not os.path.exists(filepath) for filepath in FILE_PATHS)


def test_trash_files_getter():
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


# ========== Tests for take_all ==========

def test_clean_without_take_all_flag_but_take_all_manually(take_all_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.clean([FILE_NAMES[0]], take_all=False)
    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files


def test_recover_without_take_all_flag_but_take_all_manually(take_all_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.recover([FILE_NAMES[0]], take_all=False)
    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0])
    assert os.path.exists(FILE_PATHS[0] + ' (1)')


def test_clean_with_take_all_flag():
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.clean([FILE_NAMES[0]])
    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files


def test_recover_with_take_all_flag():
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.recover([FILE_NAMES[0]])
    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0])
    assert os.path.exists(FILE_PATHS[0] + ' (1)')


def test_clean_without_take_all_flag_but_take_first_manually(take_first_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    filedict_to_be_saved = filedicts_with_name[1]

    airm.trash.clean([FILE_NAMES[0]], take_all=False)

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]

    assert len(filedicts_with_name) == 1
    assert filedicts_with_name[0] == filedict_to_be_saved


def test_recover_without_take_all_flag_but_take_first_manually(
        take_first_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    filedict_to_be_saved = filedicts_with_name[1]

    airm.trash.recover([FILE_NAMES[0]], take_all=False)

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]

    assert len(filedicts_with_name) == 1
    assert filedicts_with_name[0] == filedict_to_be_saved

    assert os.path.exists(FILE_PATHS[0])


def test_clean_without_take_all_flag_but_take_nothing_manually(
        take_nothing_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    filedict_to_be_saved = filedicts_with_name[1]

    airm.trash.clean([FILE_NAMES[0]], take_all=False)

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]

    assert len(filedicts_with_name) == 2


def test_recover_without_take_all_flag_but_take_nothing_manually(
        take_nothing_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    filedict_to_be_saved = filedicts_with_name[1]

    airm.trash.recover([FILE_NAMES[0]], take_all=False)

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]

    assert len(filedicts_with_name) == 2
    assert not os.path.exists(FILE_PATHS[0])


# ========== Tests for replace and suffix ==========

def test_recover_with_suffix():
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.recover([FILE_NAMES[0]], suffix=True)

    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0])
    assert os.path.exists(FILE_PATHS[0] + ' (1)')


def test_recover_with_replace():
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.recover([FILE_NAMES[0]], replace=True, suffix=False)

    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0])
    assert not os.path.exists(FILE_PATHS[0] + ' (1)')


def test_recover_without_replace_and_suffix_with_yes(answer_yes_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.recover([FILE_NAMES[0]], replace=False, suffix=False)

    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0])
    assert not os.path.exists(FILE_PATHS[0] + ' (1)')


def test_recover_without_replace_and_suffix_with_no(answer_no_setup):
    with open(FILE_PATHS[0], 'a'):
        pass
    airm.rm.remove([FILE_PATHS[0]])

    trash_filedicts = airm.trash.get_trash_files()
    filedicts_with_name = [filedict for filedict in trash_filedicts
                           if filedict['name'] == FILE_NAMES[0]]
    assert len(filedicts_with_name) == 2

    airm.trash.recover([FILE_NAMES[0]], replace=False, suffix=False)

    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0])
    assert os.path.exists(FILE_PATHS[0] + ' (1)')


def test_recover_with_suffix_when_there_are_already_files_with_suffix():
    files_to_create = [FILE_PATHS[0]]
    files_with_suffix = [FILE_PATHS[0] + ' ({})'.format(index)
                         for index in range(1, 30)]

    files_to_create += files_with_suffix
    for filepath in files_to_create:
        with open(filepath, 'a'):
            pass

    airm.trash.recover([FILE_NAMES[0]])

    trash_files = get_trash_filenames()
    assert FILE_NAMES[0] not in trash_files
    assert os.path.exists(FILE_PATHS[0] + ' (30)')

# ========== Tests for interactive_run ==========

def test_interactive_clean_with_yes(answer_yes_setup):
    airm.trash.clean(FILE_NAMES, interactive_run=True)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_interactive_clean_with_no(answer_no_setup):
    airm.trash.clean(FILE_NAMES, interactive_run=True)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)


def test_interactive_recover_with_yes(answer_yes_setup):
    airm.trash.recover(FILE_NAMES, interactive_run=True)
    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)

    assert all(os.path.exists(filepath) for filepath in FILE_PATHS)


def test_interactive_recover_with_no(answer_no_setup):
    airm.trash.recover(FILE_NAMES, interactive_run=True)
    trash_files = get_trash_filenames()
    assert all(filename in trash_files for filename in FILE_NAMES)

    assert all(not os.path.exists(filepath) for filepath in FILE_PATHS)


# ========== Tests for exceptions ==========

def test_clean_with_string_as_argument():
    with pytest.raises(TypeError):
        airm.trash.clean(FILE_PATHS[0])


def test_recover_with_string_as_argument():
    with pytest.raises(TypeError):
        airm.trash.recover(FILE_PATHS[0])


def test_recover_with_suffix_and_replace():
    with pytest.raises(ValueError):
        airm.trash.recover([], suffix=True, replace=True)


# ========== Tests for silent_run ==========

def test_silent_clean(capture_stdout_setup):
    airm.trash.clean(FILE_NAMES, silent_run=True)
    assert capture_stdout_setup.getvalue() == ''


def test_verbose_clean(capture_stdout_setup):
    airm.trash.clean(FILE_NAMES, silent_run=False)
    assert capture_stdout_setup.getvalue() != ''


def test_silent_recover(capture_stdout_setup):
    airm.trash.recover(FILE_NAMES, silent_run=True)
    assert capture_stdout_setup.getvalue() == ''


def test_verbose_recover(capture_stdout_setup):
    airm.trash.recover(FILE_NAMES, silent_run=False)
    assert capture_stdout_setup.getvalue() != ''


# ========== Tests for create_dir ==========

def test_recover_when_original_dir_not_exists():
    os.makedirs(DIR_PATHS[0])
    file_name = 'file_inside_dir'
    file_inside_dir = os.path.join(DIR_PATHS[0], file_name)
    with open(file_inside_dir, 'a'):
        pass

    airm.rm.remove([file_inside_dir])
    shutil.rmtree(DIR_PATHS[0])

    assert not os.path.exists(DIR_PATHS[0])
    airm.trash.recover([file_name])

    trash_files = get_trash_filenames()
    assert file_name not in trash_files
    assert os.path.exists(file_inside_dir)


def test_recover_without_create_dir_flag_but_answer_yes(answer_yes_setup):
    os.makedirs(DIR_PATHS[0])
    file_name = 'file_inside_dir'
    file_inside_dir = os.path.join(DIR_PATHS[0], file_name)
    with open(file_inside_dir, 'a'):
        pass

    airm.rm.remove([file_inside_dir])
    shutil.rmtree(DIR_PATHS[0])

    assert not os.path.exists(DIR_PATHS[0])
    airm.trash.recover([file_name], create_dir=False)

    trash_files = get_trash_filenames()
    assert file_name not in trash_files
    assert os.path.exists(file_inside_dir)


def test_recover_without_create_dir_flag_but_answer_no(answer_no_setup):
    os.makedirs(DIR_PATHS[0])
    file_name = 'file_inside_dir'
    file_inside_dir = os.path.join(DIR_PATHS[0], file_name)
    with open(file_inside_dir, 'a'):
        pass

    airm.rm.remove([file_inside_dir])
    shutil.rmtree(DIR_PATHS[0])

    assert not os.path.exists(DIR_PATHS[0])
    airm.trash.recover([file_name], create_dir=False)

    trash_files = get_trash_filenames()
    assert file_name in trash_files
    assert not os.path.exists(DIR_PATHS[0])


# ========== Tests with custom config ==========

def test_clean_with_custom_config():
    from airm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)

    airm.trash.recover(FILE_NAMES)
    airm.rm.remove(FILE_PATHS, config_path=CUSTOM_CONFIG_PATH)

    trash_files = get_trash_filenames_with_config(CUSTOM_CONFIG_PATH)
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert len(os.listdir(CUSTOM_CONFIG['trash_path'])) == 6

    airm.trash.clean(FILE_NAMES, config_path=CUSTOM_CONFIG_PATH)

    trash_files = get_trash_filenames_with_config(CUSTOM_CONFIG_PATH)
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_recover_with_custom_config():
    from airm.config import _write_config as write_config
    write_config(CUSTOM_CONFIG_PATH, CUSTOM_CONFIG)

    airm.trash.recover(FILE_NAMES)
    airm.rm.remove(FILE_PATHS, config_path=CUSTOM_CONFIG_PATH)

    trash_files = get_trash_filenames_with_config(CUSTOM_CONFIG_PATH)
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert len(os.listdir(CUSTOM_CONFIG['trash_path'])) == 6

    airm.trash.recover(FILE_NAMES, config_path=CUSTOM_CONFIG_PATH)

    trash_files = get_trash_filenames_with_config(CUSTOM_CONFIG_PATH)
    assert all(filename not in trash_files for filename in FILE_NAMES)
    assert all(os.path.exists(filepath) for filepath in FILE_PATHS)


def test_clean_with_edited_config():
    airm.trash.recover(FILE_NAMES)
    airm.rm.remove(FILE_PATHS, trash_dir=CUSTOM_CONFIG['trash_path'])

    trash_files = get_trash_filenames_with_trash_path(
        CUSTOM_CONFIG['trash_path'])
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert len(os.listdir(CUSTOM_CONFIG['trash_path'])) == 6

    airm.trash.clean(FILE_NAMES, trash_dir=CUSTOM_CONFIG['trash_path'])

    trash_files = get_trash_filenames_with_trash_path(
        CUSTOM_CONFIG['trash_path'])
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_recover_with_edited_config():
    airm.trash.recover(FILE_NAMES)
    airm.rm.remove(FILE_PATHS, trash_dir=CUSTOM_CONFIG['trash_path'])

    trash_files = get_trash_filenames_with_trash_path(
        CUSTOM_CONFIG['trash_path'])
    assert all(filename in trash_files for filename in FILE_NAMES)
    assert len(os.listdir(CUSTOM_CONFIG['trash_path'])) == 6

    airm.trash.recover(FILE_NAMES, trash_dir=CUSTOM_CONFIG['trash_path'])

    trash_files = get_trash_filenames_with_trash_path(
        CUSTOM_CONFIG['trash_path'])
    assert all(filename not in trash_files for filename in FILE_NAMES)
    assert all(os.path.exists(filepath) for filepath in FILE_PATHS)


# ========== Tests for policies ==========

def test_time_policy():
    import datetime
    orig_datetime = datetime.datetime

    # Create mocking class.
    class new_datetime:
        @staticmethod
        def now():
            return orig_datetime.now() + datetime.timedelta(days=40)

        @staticmethod
        def strptime(*args, **kwargs):
            return orig_datetime.strptime(*args, **kwargs)

    datetime.datetime = new_datetime

    airm.trash.show()

    trash_files = get_trash_filenames()
    assert len(trash_files) == 0
    assert len(os.listdir(constants.DEFAULT_TRASH_PATH)) == 1

    datetime.datetime = orig_datetime


def test_size_policy():
    airm.config.set_size_policy(1)
    airm.trash.clean([])

    files_to_create = [FILE_PATHS[0] + ' ({})'.format(index)
                         for index in range(1, 7)]

    # Create 6 files ~200KB each, whilst max trash size is 1MB.
    for filepath in files_to_create:
        with open(filepath, 'a') as infile:
            infile.write('.'*200000)

    airm.rm.remove(files_to_create)

    trash_files = get_trash_filenames()

    # 1 of 6 files must be removed by size policy, that's why length is 5.
    assert len(trash_files) == 5
    assert len(os.listdir(constants.DEFAULT_TRASH_PATH)) == 6


def test_count_policy():
    airm.config.set_count_policy(3)
    airm.trash.show()

    trash_files = get_trash_filenames()
    assert len(trash_files) == 3
    assert len(os.listdir(constants.DEFAULT_TRASH_PATH)) == 4


def test_clean_with_partial_nonexistence():
    result = airm.trash.clean(FILE_NAMES + ['nonexistent_file'])
    assert result['is_erroneous']
    assert len([error for error in result['items'] if error['msg'] != '']) == 1

    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)


def test_recover_with_partial_nonexistence():
    result = airm.trash.recover(FILE_NAMES + ['nonexistent_file'])
    assert result['is_erroneous']
    assert len([error for error in result['items'] if error['msg'] != '']) == 1

    trash_files = get_trash_filenames()
    assert all(filename not in trash_files for filename in FILE_NAMES)

    assert all(os.path.exists(filepath) for filepath in FILE_PATHS)
