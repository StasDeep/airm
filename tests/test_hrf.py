#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import pytest
import shutil

import airm

from tests.constants import TESTING_DIR_PATH, FILE_PATHS


# ===== Setup and Teardown =====

def setup_module(module):
    if not os.path.exists(TESTING_DIR_PATH):
        os.makedirs(TESTING_DIR_PATH)


def teardown_module(module):
    shutil.rmtree(TESTING_DIR_PATH)


# ===== Tests =====

def test_load():
    with open(FILE_PATHS[0], 'w') as outfile:
        outfile.write('Foo: 2\nBar: "str"\nBaz: -')

    with open(FILE_PATHS[0], 'r') as infile:
        data = airm.hrf.load(infile)

    assert data == dict(foo=2, bar='str', baz=None)


def test_loads():
    data = airm.hrf.loads('Foo: 2\nBar: "str"\nBaz: -')
    assert data == dict(foo=2, bar='str', baz=None)


def test_dump():
    data = dict(foo=2, bar='str', baz=None)

    with open(FILE_PATHS[0], 'w') as outfile:
        airm.hrf.dump(data, outfile)

    with open(FILE_PATHS[0], 'r') as infile:
        assert data == airm.hrf.load(infile)


def test_dumps():
    data = dict(foo=2, bar='str', baz=None)
    text = airm.hrf.dumps(data)
    lines = text.split('\n')
    assert set(lines) == set(['Foo: 2', 'Bar: "str"', 'Baz: -'])


def test_loads_invalid_hrf():
    with pytest.raises(ValueError):
        airm.hrf.loads('Foo: 2\nBar: str"\nBaz: -')


def test_loads_repeated_keys():
    with pytest.raises(ValueError):
        airm.hrf.loads('Foo: 2\nBaz: "str"\nBaz: -')

def test_dumps_unsupported_type():
    with pytest.raises(ValueError):
        airm.hrf.dumps(dict(foo=[]))
