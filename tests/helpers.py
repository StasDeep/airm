#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import airm


def get_trash_filenames():
    filedicts = airm.trash.get_trash_files()
    trash_filenames = [filedict['name'] for filedict in filedicts]
    return trash_filenames


def get_trash_filenames_with_config(config_path):
    filedicts = airm.trash.get_trash_files(config_path=config_path)
    trash_filenames = [filedict['name'] for filedict in filedicts]
    return trash_filenames


def get_trash_filenames_with_trash_path(trash_path):
    filedicts = airm.trash.get_trash_files(trash_dir=trash_path)
    trash_filenames = [filedict['name'] for filedict in filedicts]
    return trash_filenames
