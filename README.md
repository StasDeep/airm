# AIRM utility
AIRM is a Python script to remove files and directories.
Its main advantage is that you can easily restore your files, which have been deleted earlier.

## Installation
```
$ python setup.py install
```

## Rm Mode
In Rm Mode you can delete one or more files (move to trashbin).
```
$ airm file1 file2
```
Recursively remove by regular expression in a directory:
```
$ airm --regexp 'file\d' .
```
Remove empty directories:
```
$ airm -d empty_dir1
```
Remove non-empty directories:
```
$ airm -r nonempty_dir
```
Remove silently:
```
$ airm file1 --silent
```
Dry-run (nothing is changed):
```
$ airm file1 --dry-run
```
Interactive mode:
```
$ airm file1 file2
Remove file '/home/username/file1'? (Y/n) y
Remove file '/home/username/file2'? (Y/n) n
```
Force mode (remove write-protected without prompt):
```
$ airm file1 -f
```

## Trash Mode
In Trash Mode you can work with your deleted files. You can either recover or permanently remove them.

Clean:
```
$ airtrash -c file1
```
Recovery:
```
$ airtrash -r file2
```
Name conflict:
```
$ airtrash -c file1
Name conflict occured.
Total: 2
1. file1  file  0  2017-04-15 13:00:22
2. file1  file  0  2017-04-15 13:08:02
Please, choose file version (enter number) or leave it empty to take all or enter 0 to take nothing:
> 2
```
Automatic name conflict solving (remove all files with this name):
```
$ airtrash -c file1 --take-all
```
Clean all:
```
$ airtrash -c
```
Recover all:
```
$ airtrash -r
```
Recover to directory that does not exist:
```
$ airtrash -r file1
Directory '/home/username/original_dir' where from file was removed no longer exists. Do you want to create it? (Y/n) y
```
Automatic create-original-dir conflict solving:
```
$ airtrash -r file1 --create-dir
```
Recovering when original path already exists.
If answer is 'yes', file is replaced, otherwise suffix is added.
```
$ airtrash -r file1
File/directory '/home/username/file1' already exists. You can either replace it or add suffix to name. Do you want to replace it? (Y/n) n
```
Automatic replace-or-suffix conflict:
```
$ airtrash -r file1 --replace
```
```
$ airtrash -r file1 --suffix
```

## Config Mode
Show config:
```
$ airconfig
Count Policy: -
Size Policy: 1024
Time Policy: 30
Trash Path: "/home/stasdeep/.airm_trash"
```
Edit config:
```
$ airconfig --trash-dir newtrashpath
```
Set policy:
```
$ airconfig --time-policy 10
```
Reset policy:
```
$ airconfig --time-policy
```

## Human Readable Format
HRF is a format for storing key-value pairs.
HRF supports only integer numbers, strings and NoneType.

### Example 1
JSON:
```
{
    "foo": 1,
    "bar": "word",
    "baz": null
}
```
HRF:
```
Foo: 1
Bar: "word"
Baz: -
```

### Example 2
JSON:
```
{
    "foo_baz": 1
}
```
HRF:
```
Foo Baz: 1
```

## Help
To see other features, you can open help for any command:
```
$ airm -h
```
```
$ airtrash -h
```
```
$ airconfig -h
```

## AIRM Library
You can use AIRM as a python library.
```
import airm

airm.rm.remove(['file1'])
airm.trash.recover(['file1'])

airm.rm.remove(['file1'])
airm.trash.clean(['file1'])
```

## Features
```
- [x] Rm Mode:
  - [x] file/directory deletion;
  - [x] flags:
    - [x] --regexp;
    - [x] --force;
    - [x] --interactive;
    - [x] --recursive;
    - [x] --dir;
    - [x] --silent;
    - [x] --dry-run;
    - [x] --config-file;
- [x] Trash Mode:
  - [x] trash output:
    - [x] basic output;
    - [x] when output is larger than terminal;
  - [x] manual trash cleaning;
  - [x] automatic trash cleaning by different parameters;
  - [x] trash recovery;
  - [x] flags:
    - [x] --create-dir;
    - [x] --all;
    - [x] --suffix;
    - [x] --replace;
    - [x] --silent;
    - [x] --interactive;
    - [x] --dry-run;
    - [x] --config-file;
- [x] Config Mode:
  - [x] store trash path;
  - [x] store policies;
  - [x] human-readable format;
  - [x] flags:
    - [x] --trash-folder;
    - [x] --time-policy;
    - [x] --size-policy;
    - [x] --count-policy;
    - [x] --config-file;
    - [x] --silent;
- [x] logging;
- [x] exit code;
- [x] run as a library;
- [x] tests.
```
